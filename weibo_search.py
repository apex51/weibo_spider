# -*- coding: utf-8 -*-

import time
import datetime
import re            
import os    
import sys  
import codecs  
import shutil
import urllib 
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys        
import selenium.webdriver.support.ui as ui        
from selenium.webdriver.common.action_chains import ActionChains
from mail import send_mail, receive_mail
import random


options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('disable-gpu')
options.add_argument('no-sandbox')
driver = webdriver.Chrome(chrome_options=options)
results = []


def SetVariables(start_time_s, end_time_s):
    
    global start_time
    global end_time

    start_time = start_time_s
    end_time = end_time_s


def LoginWeibo(username, password):
    
    print('Ready to login weibo.cn ...')
    try:
        driver.get("http://login.sina.com.cn/")
    except:
        print("Failed to load http://login.sina.com.cn ..")
        return -1
    
    try:
        driver.find_element_by_xpath("//p[@class='me_name']")
    except:
        print('Not logged in, start trying ..')
    else:
        print('Already logged in, start searching ..')
        return 0

    elem_user = driver.find_element_by_name("username")
    elem_user.send_keys(username)
    elem_pwd = driver.find_element_by_name("password")
    elem_pwd.send_keys(password)
    elem_sub = driver.find_element_by_xpath("//input[@class='W_btn_a btn_34px']")
    elem_sub.click()

    while True:
        time.sleep(2)
        driver.save_screenshot("/root/weibo_spider/data/screen1.png") # for debug

        print('Current url ', driver.current_url)
        try:
            driver.find_element_by_name("door")
        except:
            try:
                driver.find_element_by_xpath("//p[@class='me_name']")
            except:
                print('still failed!')
                return -1
            else:
                return 0
        else:
            hash_str = "{:032x}".format(random.getrandbits(128))
            send_mail("random_email@126.com", "nju.jianghao@foxmail.com", "weibo_captcha:{}".format(hash_str), "", ["/root/weibo_spider/data/screen1.png"])
            # receive_mail()
            
            # 1h to listen for response
            counter = 0
            status = False
            door_code = "0000"
            while counter < 60:
                status, door_code = receive_mail(hash_str) # receive contain
                if status:
                    break;
                counter += 1
                time.sleep(60)
            
            if status:
                elem_door = driver.find_element_by_name("door")
                elem_door.clear()
                elem_door.send_keys(door_code)
                driver.save_screenshot("/root/weibo_spider/data/screen2.png") # for debug
                elem_sub.click()
                time.sleep(2)
                try:
                    driver.find_element_by_xpath("//p[@class='me_name']")
                except:
                    print("Try another round.")
                else:
                    print("Succeded to log in.")
                    send_mail("random_email@126.com", "nju.jianghao@foxmail.com", "succeeded:weibo_captcha:{}".format(hash_str), "")
                    return 0

            else:
                print("Failed to log in after 1 hour of no action.")
                return -1
            
    # time.sleep(2)
    # try:
    #     driver.find_element_by_xpath("//p[@class='me_name']")
    # except:
    #     print("Failed to log in.")
    #     return -1
    # else:
    #     print("Succeded to log in.")
    #     #获取 Coockie 参考：http://www.cnblogs.com/fnng/p/3269450.html
    #     print('Cookie Information:')
    #     for cookie in driver.get_cookies(): 
    #         print(cookie)
    #         for key in cookie:
    #             print(key, cookie[key]) 
    #     return 0


def GetSearchContent(key):

    time.sleep(2)
    driver.get("http://s.weibo.com/")
    print('搜索热点主题：', key)

    #输入关键词并点击搜索
    item_inp = driver.find_element_by_xpath("//input[@class='searchInp_form']")
    item_inp.send_keys(key)
    item_inp.send_keys(Keys.RETURN)    #采用点击回车直接搜索

    #获取搜索词的URL，用于后期按时间查询的URL拼接
    current_url = driver.current_url
    current_url = current_url.split('&')[0] #http://s.weibo.com/weibo/%25E7%258E%2589%25E6%25A0%2591%25E5%259C%25B0%25E9%259C%2587

    global page
    global start_time
    global end_time
    global outfile
    global sheet
    global results

    # outfile = xlwt.Workbook(encoding = 'utf-8')

    page = 1
    results = []

    # 每一天使用一个sheet存储数据
    # sheet = outfile.add_sheet(end_time)
    # initXLS()

    #通过构建URL实现每一天的查询
    url = current_url + '&scope=ori&suball=1&timescope=custom:' + start_time + ':' + end_time + '&Refer=g'
    driver.get(url)
    
    handlePage()  #处理当前页面内容

    return results

#页面加载完成后，对页面内容进行处理
def handlePage():
    while True:
        #之前认为可能需要sleep等待页面加载，后来发现程序执行会等待页面加载完毕
        #sleep的原因是对付微博的反爬虫机制，抓取太快可能会判定为机器人，需要输入验证码
        #休息更长的随机时间
        sleep_time = 2 + random.randint(1, 10)
        time.sleep(sleep_time)
        #先行判定是否有内容
        if checkContent():
            print("getContent")
            getContent()
            #先行判定是否有下一页按钮
            if checkNext():
                #拿到下一页按钮
                next_page_btn = driver.find_element_by_xpath("//a[@class='page next S_txt1 S_line1']")
                next_page_btn.click()
            else:
                print("no Next")
                break
        else:
            print("no Content")
            break

#判断页面加载完成后是否有内容
def checkContent():
    #有内容的前提是有“导航条”？错！只有一页内容的也没有导航条
    #但没有内容的前提是有“pl_noresult”
    try:
        driver.find_element_by_xpath("//div[@class='pl_noresult']")
        flag = False
    except:
        flag = True
    return flag

#判断是否有下一页按钮
def checkNext():
    try:
        driver.find_element_by_xpath("//a[@class='page next S_txt1 S_line1']")
        flag = True
    except:
        flag = False
    return flag

# #在添加每一个sheet之后，初始化字段
# def initXLS():

#     pass
#     # name = ['博主昵称', '微博认证', '粉丝数', '微博内容', '发布时间', '转发', '评论', '赞']

#     # print 'Init XLS'
    
#     # global row
#     # global outfile
#     # global sheet
#     # global filename

#     # row  = 0
#     # for i in range(len(name)):
#     #     sheet.write(row, i, name[i])
#     # row = row + 1
    
#     # if os.path.exists(filename):
#     #     os.remove(filename)
    
#     # outfile.save(filename)

# #将dic中的内容写入excel
# def writeXLS(dic):
    
#     global row
#     global outfile
#     global sheet
#     global filename

#     pass

#     # for k in dic:
#     #     print dic[k]
#     #     sheet.write(row, 0, xlwt.Formula(u'HYPERLINK("{}", "{}")'.format(dic[k][1], dic[k][0]))) #博主
#     #     sheet.write(row, 1, dic[k][2]) #粉丝数
#     #     sheet.write(row, 2, dic[k][3]) #微博认证
#     #     sheet.write(row, 3, xlwt.Formula(u'HYPERLINK("{}", "{}")'.format(dic[k][5], dic[k][4]))) #微博内容
#     #     sheet.write(row, 4, dic[k][6]) #发布时间
#     #     sheet.write(row, 5, dic[k][7]) #转发
#     #     sheet.write(row, 6, dic[k][8]) #评论
#     #     sheet.write(row, 7, dic[k][9]) #评论
#     #     row = row + 1
#     # outfile.save(filename)


#在页面有内容的前提下，获取内容
def getContent():

    #寻找到每一条微博的class
    nodes = driver.find_elements_by_xpath("//div[@class='WB_cardwrap S_bg2 clearfix']")

    #在运行过程中微博数==0的情况，可能是微博反爬机制，需要输入验证码
    if len(nodes) == 0:
        # raw_input("请在微博页面输入验证码！")
        # url = driver.current_url
        # driver.get(url)
        # getContent()
        return

    global page
    global results
    print('results 长度', len(results))
    print(start_time)
    print('页数:', page)
    page = page + 1
    print('微博数量', len(nodes))

    for i in range(len(nodes)):
        result = []
        
        try:
            BZNC = nodes[i].find_element_by_xpath(".//div[@class='feed_content wbcon']/a[@class='W_texta W_fb']").text
        except:
            BZNC = ''
        print('博主昵称:', BZNC)
        result.append(BZNC)

        try:
            BZZY = nodes[i].find_element_by_xpath(".//div[@class='feed_content wbcon']/a[@class='W_texta W_fb']").get_attribute("href")
        except:
            BZZY = ''
        print('博主主页:', BZZY)
        result.append(BZZY)

        try:
            WBRZ = nodes[i].find_element_by_xpath(".//div[@class='feed_content wbcon']/a[contains(@class, 'W_icon')]").get_attribute('title')#若没有认证则不存在节点
        except:
            WBRZ = ''
        print('微博认证:', WBRZ)
        result.append(WBRZ)

        #鼠标悬停，更新页面代码
        try:
            element_to_hover_over = nodes[i].find_element_by_xpath(".//img[@class='W_face_radius']")
            hover = ActionChains(driver).move_to_element(element_to_hover_over)
            hover.perform()
            time.sleep(5)
            #粉丝数
            FAN_NUM = 0
            try:
                FAN_NUM = int(driver.find_element_by_xpath("//li[a[@suda-uatrack='key=tblog_usercard_new&value=chick_fansnum']]").text.replace(u'粉丝', '').replace(u'万', '0000'))
            except e:
                print(e)
        except:
            print('获取用户卡片失败')
        print('粉丝数:', FAN_NUM)
        result.append(FAN_NUM)

        try:
            WBNR = nodes[i].find_element_by_xpath(".//div[@class='feed_content wbcon']/p[@class='comment_txt']").text
        except:
            WBNR = ''
        print('微博内容:', WBNR)
        result.append(WBNR)

        try:
            WBDZ = nodes[i].find_element_by_xpath(".//div[@class='feed_from W_textb']/a[@class='W_textb']").get_attribute("href")
        except:
            WBDZ = ''
        print('微博地址:', WBDZ)
        result.append(WBDZ)

        try:
            FBSJ = nodes[i].find_element_by_xpath(".//div[@class='feed_from W_textb']/a[@class='W_textb']").text
        except:
            FBSJ = ''
        print('发布时间:', FBSJ)
        result.append(FBSJ)

        try:
            ZF_TEXT = nodes[i].find_element_by_xpath(".//a[@action-type='feed_list_forward']//em").text
            if ZF_TEXT == '':
                ZF = 0
            else:
                ZF = int(ZF_TEXT)
        except:
            ZF = 0
        print('转发:', ZF)
        result.append(ZF)

        try:
            PL_TEXT = nodes[i].find_element_by_xpath(".//a[@action-type='feed_list_comment']//em").text#可能没有em元素
            if PL_TEXT == '':
                PL = 0
            else:
                PL = int(PL_TEXT)
        except:
            PL = 0
        print('评论:', PL)
        result.append(PL)

        try:
            ZAN_TEXT = nodes[i].find_element_by_xpath(".//a[@action-type='feed_list_like']//em").text #可为空
            if ZAN_TEXT == '':
                ZAN = 0
            else:
                ZAN = int(ZAN_TEXT)
        except:
            ZAN = 0
        print('赞:', ZAN)
        result.append(ZAN)

        results.append(result)

        print('\n')

        

    # # #写入Excel
    # # writeXLS(dic)
    # return dic
