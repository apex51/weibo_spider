# -*- coding: utf-8 -*-

from weibo_search import LoginWeibo
from weibo_search import GetSearchContent
from weibo_search import SetVariables
from mail import send_mail
from datetime import datetime, timedelta
import sys
import xlwt
import os

if __name__ == '__main__':

    #定义变量
    username = 'tommyxxx@126.com'
    password = 'tommyxxx223344'
    LoginWeibo(username, password) #登陆微博

    print("test done")

    #设置 xls 文件位置
    filename = "/root/weibo_spider/data/crawl_output.xls"
    stop_times = ["12", "17", "20", "08"]
    durations = [4, 5, 3, 12]
    idx = int(sys.argv[1])

    # stop_time = datetime.now().strftime('%Y-%m-%d') + "-" + "04"
    # start_time = (datetime.strptime(stop_time, '%Y-%m-%d-%H') - timedelta(hours=1)).strftime('%Y-%m-%d-%H')
    stop_time = datetime.now().strftime('%Y-%m-%d') + "-" + stop_times[idx]
    start_time = (datetime.strptime(stop_time, '%Y-%m-%d-%H') - timedelta(hours=durations[idx])).strftime('%Y-%m-%d-%H')
    stop_time_nice = datetime.strptime(stop_time, '%Y-%m-%d-%H').strftime('%Y-%m-%d %H:%M:%S')
    start_time_nice = datetime.strptime(start_time, '%Y-%m-%d-%H').strftime('%Y-%m-%d %H:%M:%S')
    SetVariables(start_time, stop_time)

    #搜索热点微博 爬取评论
    search_keys = ["房车~营地~自驾", "狗 旅行"]
    # search_keys = ["房车~营地~自驾"]

    search_results = {}
    for key in search_keys:
        search_results[key] = GetSearchContent(key)

    # excel
    if os.path.exists(filename):
        os.remove(filename)
    
    outfile = xlwt.Workbook(encoding='utf-8')
    sheet = outfile.add_sheet(stop_time)
    name = ['关键词', '博主昵称', '微博认证', '粉丝数', '微博内容', '链接', '发布时间', '转发', '评论', '赞']
    row  = 0
    for i in range(len(name)):
        sheet.write(row, i, name[i])

    search_key_list = " ".join(search_keys).replace("~", " ").split()
    for (key, results) in search_results.items():
        for result in results:
            row += 1
            sheet.write(row, 0, ",".join([key for key in search_key_list if key in result[4]]))
            sheet.write(row, 1, result[0])
            sheet.write(row, 2, result[2])
            sheet.write(row, 3, result[3])
            sheet.write(row, 4, result[4])
            sheet.write(row, 5, xlwt.Formula(u'HYPERLINK("{}", "{}")'.format(result[5], result[5])))
            sheet.write(row, 6, result[6])
            sheet.write(row, 7, result[7])
            sheet.write(row, 8, result[8])
            sheet.write(row, 9, result[9])
    
    outfile.save(filename)

    send_from = "random_email@126.com"
    send_to = "nju.jianghao@foxmail.com" #用逗号分隔
    subject = "微博搜索：{} 到 {}".format(start_time_nice, stop_time_nice)
    text = "共 {} 条搜索结果".format(row)
    #发送邮件
    send_mail(send_from, send_to, subject, text, files=[filename])
