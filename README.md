## A spider for weibo keywords search.

This project is mainly refered from [weibo_search_spider](https://github.com/zhiyxu/weibo_search_spider). It's written to help Yuan to get rid of the annoying daily search work. In order to make this an automated tool, the following points need to be done.

- Email the file as attachment.
- Deploy to the server side using crontab.
- Change to Python 3 for better codec.
- Recontruction and modulization.

### Environment

- Python 2.7
- selenium 3.9.0
- xlwt 1.3.0
