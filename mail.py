import smtplib
import imaplib
import email
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate


mail_username = "random_email@126.com"
mail_password = "tommyxxx223344"
smtp_server = "smtp.126.com"
imap_server = "imap.126.com"


def send_mail(send_from, send_to, subject, text, files=None):
    
    # assert isinstance(send_to, list)

    msg = MIMEMultipart('alternative')
    msg['From'] = send_from
    msg['To'] = send_to
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject
    msg.attach(MIMEText(text, 'plain'))

    for f in files or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(fil.read(), Name=basename(f))
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)

    smtp = smtplib.SMTP_SSL(smtp_server)
    smtp.login(mail_username, mail_password)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def receive_mail(hash_str):
    # try to fetch the newest mail subject. Retry 20 times.
    conn = imaplib.IMAP4_SSL(imap_server)
    try:
        (retcode, capabilities) = conn.login(mail_username, mail_password)
    except:
        print('failed to login to email')

    conn.select('INBOX', readonly=1)
    (retcode, messages) = conn.search(None, 'SUBJECT', '"{}"'.format(hash_str))

    if messages[0]:
        num = messages[0].split()[0]
        typ, data = conn.fetch(num, '(RFC822)')
        msg = email.message_from_bytes(data[0][1])
        door_code = msg['subject'].split(':')[-1]
        return (True, door_code)
    else:
        return (False, "0000")

    conn.close()
